import { getEnvironmentString } from '@kominal/lib-node-environment';

export const STACK_NAME = getEnvironmentString('STACK_NAME');
