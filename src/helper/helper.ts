import { Certificate } from '../models/certificate';

export function matchCertificate(existingCertificate: Certificate, requiredCertificate: Certificate) {
	return (
		existingCertificate.domain === requiredCertificate.domain &&
		existingCertificate.cert === requiredCertificate.cert &&
		existingCertificate.key === requiredCertificate.key
	);
}

export function buildStore(certificates: Certificate[]) {
	return {
		https: {
			Certificates: certificates.map((c) => {
				return {
					domain: {
						main: c.domain,
					},
					certificate: c.cert,
					key: c.key,
					Store: 'default',
				};
			}),
		},
	};
}
