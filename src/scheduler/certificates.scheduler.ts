import { Scheduler } from '@kominal/lib-node-scheduler';
import { info } from '@kominal/observer-node-client';
import Dockerode from 'dockerode';
import { readFileSync, writeFileSync } from 'fs';
import { STACK_NAME } from '../helper/environment';
import { buildStore, matchCertificate } from '../helper/helper';
import { Certificate } from '../models/certificate';
import { CertificateDatabase } from '../models/certificate.database';

const docker = new Dockerode({ socketPath: '/var/run/docker.sock' });
const PATH = '/opt/traefik/certificates.json';

export class CertificatesScheduler extends Scheduler {
	constructor() {
		super();
	}

	async run(): Promise<void> {
		info('Running scheduler...');
		const certificates = await CertificateDatabase.find();

		let content: any = {};
		try {
			content = JSON.parse(readFileSync(PATH, 'utf8'));
		} catch (e) {
			info(e);
		}

		const certificateList: any[] = content.https.Certificates;
		const existingCerts = certificateList.map((cert) => {
			return {
				domain: cert.domain.main,
				cert: cert.certificate,
				key: cert.key,
			} as Certificate;
		});

		let updateRequired = false;
		for (const requiredCertificate of certificates) {
			if (!existingCerts.find((existingCertificate) => matchCertificate(existingCertificate, requiredCertificate))) {
				info(`Missing certificate for domain: ${requiredCertificate.domain}`);
				updateRequired = true;
			}
		}

		if (updateRequired) {
			info('Certificates need to be replaced.');
			const store = buildStore(await CertificateDatabase.find());
			const requiredContent = JSON.stringify(store);
			writeFileSync(PATH, requiredContent);
			const containers = await docker.listContainers({
				filters: { label: [`com.docker.swarm.service.name=${STACK_NAME}_loadbalancer`] },
			});
			for (const containerInfo of containers) {
				const container = docker.getContainer(containerInfo.Id);
				await container.remove({ force: true });
			}
		} else {
			info('Certificates are ok.');
		}
	}
}
