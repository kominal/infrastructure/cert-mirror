export interface Certificate {
	domain: string;
	csr: string;
	key: string;
	cert: string;
	expires: Date;
	renew: boolean;
}
