import { Document, model, Schema } from 'mongoose';
import { Certificate } from './certificate';

export const CertificateDatabase = model<Document & Certificate>(
	'Certificate',
	new Schema(
		{
			domain: String,
			csr: String,
			key: String,
			cert: String,
			expires: Date,
			renew: Boolean,
		},
		{ minimize: false }
	)
);
