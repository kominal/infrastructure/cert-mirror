import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { error, info, startObserver } from '@kominal/observer-node-client';
import { exit } from 'process';
import { CertificatesScheduler } from './scheduler/certificates.scheduler';

let mongoDBInterface: MongoDBInterface;

async function start() {
	try {
		startObserver();
		mongoDBInterface = new MongoDBInterface('certificates');
		await mongoDBInterface.connect();
		await new CertificatesScheduler().start(3600);
	} catch (e) {
		error(`Failed to start service: ${e}`);
		exit(1);
	}
}
start();

process.on('SIGTERM', async () => {
	info(`Received system signal 'SIGTERM'. Shutting down service...`);
	await mongoDBInterface.disconnect();
});
